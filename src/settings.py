import logging.config
from os import path

"""---------------------------- PATHS AND FOLDERS ---------------------------"""
PROJECT_ROOT = path.normpath(path.join(path.dirname(path.realpath(__file__)), '..'))
LOGS_DEBUG_FILE = path.join(PROJECT_ROOT, 'resources', 'app.log')
LOGS_ERROR_FILE = path.join(PROJECT_ROOT, 'resources', 'error.log')
SLEEP_AFTER_BITFINEX_REQUEST = 2  # Recommended 2 second
SLEEP_AFTER_BINANCE_REQUEST = 0.1  # Recommended 0.1 second

DATABASE = "sqlite:///"+PROJECT_ROOT+"/resources/database.sqlite"

SETTINGS = {}

"""---------------------------- LOGGER CONFIG ---------------------------"""
logging.config.dictConfig({
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'default': {
            'format': '%(asctime)s-%(name)s-%(levelname)s-%(message)s'
        },
    },
    'handlers': {
        'console': {
            'level': 'INFO',
            'formatter': 'default',
            'class': 'logging.StreamHandler',
        },
        'console_backend': {
            'level': 'INFO',
            'formatter': 'default',
            'class': 'logging.StreamHandler',
        },
        'deb_file': {
            'level': 'DEBUG',
            'formatter': 'default',
            'class': 'logging.handlers.RotatingFileHandler',
            'maxBytes': 10485760,  # 10MB
            'backupCount': 5,
            'encoding': 'utf8',
            'filename': path.join(PROJECT_ROOT, 'logs', LOGS_DEBUG_FILE)
        },
        'err_file': {
            'level': 'ERROR',
            'formatter': 'default',
            'class': 'logging.handlers.RotatingFileHandler',
            'maxBytes': 10485760,  # 10MB
            'backupCount': 5,
            'encoding': 'utf8',
            'filename': path.join(PROJECT_ROOT, 'logs', LOGS_ERROR_FILE)
        },
    },
    'loggers': {
        '': {
            'handlers': ['deb_file', 'err_file', 'console'],
            'level': 'DEBUG',
            'propagate': False
        },
    }
})

try:
    from src.local_settings import *
except ImportError:
    pass
