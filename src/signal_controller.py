import datetime
import logging

import pytz

from indicators.ao import AO
from indicators.zigzag import ZigZag
from indicators.hma import HMA
from indicators.hmacci import HMACCI
from indicators.hmadir import HMADIR
from indicators.hmao import HMAO
from indicators.ma2oc import MA2OC
from indicators.sma import SMA
from src.helpers import combine_dict_candle
from src.models import SignalAction, StrategyType

logger = logging.getLogger(__name__)


class SignalFormer:
    def __init__(self, candles, periods, settings):
        self.candles = candles
        self.periods = periods
        self.settings = settings
        self.start_date = self.settings['start_date'].replace(tzinfo=pytz.utc)
        self.indicators = []
        self.strategy_type = self.settings['strategy_type']
        self.timeframe = self.settings['timeframe']
        self.alt_timeframe = self.settings.get('alt_timeframe', None)

        if len(periods) >= 1:
            self.ma2oc = MA2OC(periods[0])
            self.hmadir = HMADIR(periods[0])
            self.hma_hl2 = HMA(periods[0], source='hl2')
            self.sma = SMA(periods[0], source="open")
            self.hma = HMA(periods[0], source="open")
            self.zigzag = ZigZag(periods[0], self.timeframe, self.alt_timeframe)

        if len(periods) >= 2:
            self.hmao = HMAO(periods[0], periods[1])
            self.hmacci = HMACCI(periods[1])
            self.ao = AO(periods[0], periods[1], source='hl2')

        self.position = max(periods)
        self.candle_group = []
        self.alt_candle_group = []
        self.pyramiding_group = []

        self.position = 1
        self.last_not_hold_signal = None
        self.pre_last_not_hold_signal = None
        self.last_opposite_signal = None
        self.stop_price = None
        self.signal_group = []

    def compute_indicator(self):
        if self.strategy_type is StrategyType.han_solo_3:
            hmao_value = self.hmao.value()
            hmacci_value = self.hmacci.value()
            return hmao_value, hmacci_value
        elif self.strategy_type is StrategyType.ao:
            ao_value = self.ao.value()
            return ao_value,
        elif self.strategy_type is StrategyType.ma2oc:
            ma2oc_value = self.ma2oc.value()
            return ma2oc_value,
        elif self.strategy_type is StrategyType.han_solo_jr or self.strategy_type is StrategyType.han_solo_sr:
            hmao_value = self.hmao.value()
            hmacci_value = self.hmacci.value()
            hmadir_value = self.hmadir.value()
            return hmao_value, hmacci_value, hmadir_value
        elif self.strategy_type is StrategyType.werewolf_basic or self.strategy_type is StrategyType.werewolf_ntwr:
            hma_value = self.hma_hl2.value()
            return hma_value,
        elif (self.strategy_type is StrategyType.enod_low_sma or
              self.strategy_type is StrategyType.enod_low_avg or
              self.strategy_type is StrategyType.enod_low_avg_simple or
              self.strategy_type is StrategyType.enod_high_sma or
              self.strategy_type is StrategyType.enod_high_avg):

            sma_value = self.sma.value()
            hma_value = self.hma.value()
            return sma_value, hma_value
        elif self.strategy_type is StrategyType.zig_zag:
            zig_zag_value = self.zigzag.value()
            return zig_zag_value

        else:
            raise TypeError("Invalid strategy type")

    def generate_signals(self):
        signals = []

        start_date = self.start_date - datetime.timedelta(minutes=self.timeframe * max(self.periods) + self.timeframe)
        next_end_date = start_date + datetime.timedelta(minutes=self.timeframe)
        if next_end_date.day != start_date.day:
            next_end_date = next_end_date.replace(hour=0, minute=0, second=0, microsecond=0)

        if self.alt_timeframe is not None:
            alt_start_date = self.start_date - datetime.timedelta(minutes=self.alt_timeframe * max(self.periods) + self.alt_timeframe)
            alt_next_end_date = alt_start_date + datetime.timedelta(minutes=self.alt_timeframe)
            if alt_next_end_date.day != alt_start_date.day:
                alt_next_end_date = alt_next_end_date.replace(hour=0, minute=0, second=0, microsecond=0)

        checked = 0
        for candle in self.candles:
            if start_date <= candle['open_date_dt'] < next_end_date:
                self.candle_group.append(candle)
                # check for stop signal
                if self.settings['trailing_stop_perc'] and self.last_not_hold_signal is not None and self.last_not_hold_signal[0] is not SignalAction.stop:
                    checked += 1
                    stop_signal = self.check_stop(candle)
                    if stop_signal is not None:
                        self.pre_last_not_hold_signal = self.last_not_hold_signal
                        self.last_not_hold_signal = stop_signal
                        signals.append(stop_signal)
            else:
                checked = 0
                # every timeframe, generate timeframe_candle
                timeframe_candle = combine_dict_candle(self.candle_group, self.timeframe) if self.candle_group else None
                if self.candle_group:
                    start_date = self.candle_group[-1]['open_date_dt'] + datetime.timedelta(minutes=1)
                    next_end_date = start_date + datetime.timedelta(minutes=self.timeframe)
                    if next_end_date.day != start_date.day:
                        next_end_date = next_end_date.replace(hour=0, minute=0, second=0, microsecond=0)

                self.candle_group.clear()
                self.candle_group.append(candle)

                if timeframe_candle:
                    # compute indicator
                    if self.strategy_type is StrategyType.han_solo_3:
                        self.hmacci.add(timeframe_candle)
                        self.hmao.add(timeframe_candle)

                    elif self.strategy_type is StrategyType.ao:
                        self.ao.add(timeframe_candle)

                    elif self.strategy_type is StrategyType.ma2oc:
                        self.ma2oc.add(timeframe_candle)

                    elif self.strategy_type is StrategyType.han_solo_jr or self.strategy_type is StrategyType.han_solo_sr:
                        self.hmao.add(timeframe_candle)
                        self.hmacci.add(timeframe_candle)
                        self.hmadir.add(timeframe_candle)
                    elif self.strategy_type is StrategyType.werewolf_basic or self.strategy_type is StrategyType.werewolf_ntwr:
                        self.hma_hl2.add(timeframe_candle)

                    elif (self.strategy_type is StrategyType.enod_low_sma or
                          self.strategy_type is StrategyType.enod_low_avg or
                          self.strategy_type is StrategyType.enod_high_sma or
                          self.strategy_type is StrategyType.enod_low_avg_simple or
                          self.strategy_type is StrategyType.enod_high_avg):
                        self.sma.add(timeframe_candle)
                        self.hma.add(timeframe_candle)

                    elif self.strategy_type is StrategyType.zig_zag:
                        self.zigzag.add(timeframe_candle)

                    if self.strategy_type is not StrategyType.zig_zag:
                        indicators = self.compute_indicator()
                        indicator = indicators + (timeframe_candle,)
                        self.indicators.append(indicator)

                    # create signal
                    if len(self.indicators) > 1:
                        signal = self.create_signal(indicator)
                        if signal[0] is not SignalAction.hold:
                            # add new signal to signal group
                            if self.last_not_hold_signal and self.last_not_hold_signal[0] is not signal[0]:
                                self.signal_group.clear()
                            self.signal_group.append(signal)

                            # update stop price for signal group
                            if self.settings['stop_variant'] == "AVG":
                                if signal[0] is SignalAction.buy:
                                    current_price = sum([s[1] for s in self.signal_group]) / len(self.signal_group)
                                    new_stop_price = current_price - current_price * (self.settings['trailing_stop_perc'] / 100)
                                    self.stop_price = new_stop_price

                                elif signal[0] is SignalAction.sell:
                                    current_price = sum([s[1] for s in self.signal_group]) / len(self.signal_group)
                                    new_stop_price = current_price + current_price * (self.settings['trailing_stop_perc'] / 100)
                                    self.stop_price = new_stop_price
                            elif self.settings['stop_variant'] == "MAX":
                                if signal[0] is SignalAction.buy:
                                    current_price = max([s[1] for s in self.signal_group])
                                    new_stop_price = current_price - current_price * (self.settings['trailing_stop_perc'] / 100)
                                    self.stop_price = new_stop_price

                                elif signal[0] is SignalAction.sell:
                                    current_price = min([s[1] for s in self.signal_group])
                                    new_stop_price = current_price + current_price * (self.settings['trailing_stop_perc'] / 100)
                                    self.stop_price = new_stop_price
                            elif self.settings['stop_variant'] == "STAIRS_MAX":
                                if signal[0] is SignalAction.buy:
                                    current_price = max([s[1] for s in self.signal_group])
                                    new_stop_price = current_price - current_price * ((self.settings['trailing_stop_perc']) * (self.settings['pyramiding'] - (len(self.signal_group) - 1)) / 100)
                                    self.stop_price = new_stop_price

                                elif signal[0] is SignalAction.sell:
                                    current_price = min([s[1] for s in self.signal_group])
                                    new_stop_price = current_price + current_price * ((self.settings['trailing_stop_perc']) * (self.settings['pyramiding'] - (len(self.signal_group) - 1)) / 100)
                                    self.stop_price = new_stop_price
                            elif self.settings['stop_variant'] == "STAIRS_AVG":
                                if signal[0] is SignalAction.buy:
                                    current_price = sum([s[1] for s in self.signal_group]) / len(self.signal_group)
                                    new_stop_price = current_price - current_price * ((self.settings['trailing_stop_perc']) * (self.settings['pyramiding'] - (len(self.signal_group) - 1)) / 100)
                                    self.stop_price = new_stop_price

                                elif signal[0] is SignalAction.sell:
                                    current_price = sum([s[1] for s in self.signal_group]) / len(self.signal_group)
                                    new_stop_price = current_price + current_price * ((self.settings['trailing_stop_perc']) * (self.settings['pyramiding'] - (len(self.signal_group) - 1)) / 100)
                                    self.stop_price = new_stop_price
                            self.pre_last_not_hold_signal = self.last_not_hold_signal
                            self.last_not_hold_signal = signal
                            signals.append(signal)
                            self.stop_price = None
                        self.position += 1

            # get alt timeframe candle
            if self.alt_timeframe is not None:
                if alt_start_date <= candle['open_date_dt'] < alt_next_end_date:
                    self.alt_candle_group.append(candle)
                else:
                    alt_timeframe_candle = combine_dict_candle(self.alt_candle_group, self.alt_timeframe) if self.alt_candle_group else None
                    if self.alt_candle_group:
                        alt_start_date = self.alt_candle_group[-1]['open_date_dt'] + datetime.timedelta(minutes=1)
                        alt_next_end_date = alt_start_date + datetime.timedelta(minutes=self.alt_timeframe)
                        if alt_next_end_date.day != alt_start_date.day:
                            alt_next_end_date = alt_next_end_date.replace(hour=0, minute=0, second=0, microsecond=0)

                    self.alt_candle_group.clear()
                    self.alt_candle_group.append(candle)

                    if alt_timeframe_candle:
                        if self.strategy_type is StrategyType.zig_zag:
                            self.zigzag.add(alt_timeframe_candle, is_alt=True)

                            indicators = self.compute_indicator()
                            indicator = indicators + (alt_timeframe_candle,)
                            self.indicators.append(indicator)
                    # # check for stop signal
                    # if self.settings['trailing_stop_perc']:
                    #     stop_signal = self.check_stop(candle)
                    #     if stop_signal is not None:
                    #         self.pre_last_not_hold_signal = self.last_not_hold_signal
                    #         self.last_not_hold_signal = stop_signal
                    #         signals.append(stop_signal)
        if self.start_date:
            filtered_signals = []
            for signal in signals:
                if signal[2] >= self.start_date:
                    filtered_signals.append(signal)
            return filtered_signals
        else:
            return signals

    def check_stop(self, candle):
        # every one-minute candle
        if self.last_not_hold_signal[0] is SignalAction.buy:
            # no loss stop price
            if self.settings['min_profit_for_stop'] and self.settings['profit_price_divider'] and self.signal_group:
                current_price = sum([s[1] for s in self.signal_group]) / len(self.signal_group)
                profit = ((candle['high'] - current_price) / current_price) * 100
                profit_price = candle['high'] * (profit / 100)
                new_stop_price = self.last_not_hold_signal[1] + profit_price / self.settings['profit_price_divider']
                if profit >= self.settings['min_profit_for_stop'] and self.stop_price is not None:
                    if new_stop_price > self.stop_price:
                        self.stop_price = new_stop_price
            else:
                # update stop price
                new_stop_price = candle['high'] - candle['high'] * (self.settings['trailing_stop_perc'] / 100)
                if self.stop_price is None or new_stop_price > self.stop_price:
                    self.stop_price = new_stop_price
            # create stop signal
            if self.stop_price is not None and candle['low'] <= self.stop_price:
                signal = self.create_stop_signal(self.stop_price, candle['open_date_dt'])
                return signal

        if self.last_not_hold_signal[0] is SignalAction.sell:
            # no loss stop price
            if self.settings['min_profit_for_stop'] and self.settings['profit_price_divider'] and self.signal_group:
                current_price = sum([s[1] for s in self.signal_group]) / len(self.signal_group)
                profit = (((current_price - candle['low']) / candle['low']) * 100)
                profit_price = (candle['low'] * (profit / 100))
                new_stop_price = self.last_not_hold_signal[1] - profit_price / self.settings['profit_price_divider']
                if profit >= self.settings['min_profit_for_stop'] and self.stop_price is not None:
                    if new_stop_price < self.stop_price:
                        self.stop_price = new_stop_price
            else:
                # update stop price
                new_stop_price = candle['low'] + candle['low'] * (self.settings['trailing_stop_perc'] / 100)
                if self.stop_price is None or new_stop_price < self.stop_price:
                    self.stop_price = new_stop_price

            # create stop signal
            if self.stop_price is not None and candle['high'] >= self.stop_price:
                signal = self.create_stop_signal(self.stop_price, candle['open_date_dt'])
                return signal

    def create_stop_signal(self, price, date):
        # calc profit for signal
        profit = 0
        if self.last_not_hold_signal and self.last_not_hold_signal[0] is not SignalAction.stop:
            _, p_price, _, _, _, _ = self.last_not_hold_signal
            if self.last_not_hold_signal[0] is SignalAction.sell:
                profit = ((p_price - price) / p_price) * 100
            elif self.last_not_hold_signal[0] is SignalAction.buy:
                profit = ((price - p_price) / p_price) * 100

        # action, price, date, indicator, level, profit
        signal = (SignalAction.stop, price, date, 0, 1, profit)
        return signal

    def create_signal(self, indicator):
        if self.strategy_type is StrategyType.han_solo_3:
            p_indicator = self.indicators[self.position - 1]
            p_hmao, p_hmacci, _ = p_indicator
            hmao, hmacci, candle = indicator

            if hmacci > 0 and (hmao > p_hmao):
                action = SignalAction.buy
            elif hmacci < 0 and (hmao < p_hmao):
                action = SignalAction.sell
            else:
                action = SignalAction.hold

        elif self.strategy_type is StrategyType.ao:
            p_indicator = self.indicators[self.position - 1]
            p_ao, _ = p_indicator
            ao, candle = indicator

            if ao < p_ao:
                action = SignalAction.sell
            elif ao > p_ao:
                action = SignalAction.buy
            else:  # ao == p_ao
                action = SignalAction.hold

        elif self.strategy_type is StrategyType.ma2oc:
            p_indicator = self.indicators[self.position - 1]
            p_ma2oc, _ = p_indicator
            ma2oc, candle = indicator

            if ((self.last_not_hold_signal is not None and self.last_not_hold_signal[0] == SignalAction.buy) or self.last_not_hold_signal is None) and p_ma2oc >= 0 and ma2oc < 0:
                action = SignalAction.sell
            elif ((self.last_not_hold_signal is not None and self.last_not_hold_signal[0] == SignalAction.sell) or self.last_not_hold_signal is None) and p_ma2oc <= 0 and ma2oc > 0:
                action = SignalAction.buy
            else:
                action = SignalAction.hold

        elif self.strategy_type is StrategyType.han_solo_jr:
            p_indicator = self.indicators[self.position - 1]
            p_hmao, p_hmacci, p_hmadir, _ = p_indicator
            hmao, hmacci, hmadir, candle = indicator

            if (hmacci > 0 and (hmao > p_hmao)) or \
                    (hmacci > 0 and hmadir > 0) or \
                    ((hmao > p_hmao) and hmadir > 0) or \
                    (hmacci > 0 and (hmao > p_hmao) and hmadir > 0):
                action = SignalAction.buy
            elif (hmacci < 0 and (hmao < p_hmao)) or \
                    (hmacci < 0 and hmadir < 0) or \
                    ((hmao < p_hmao) and hmadir < 0) or \
                    (hmacci < 0 and (hmao < p_hmao) and hmadir < 0):
                action = SignalAction.sell
            else:
                action = SignalAction.hold

        elif self.strategy_type is StrategyType.han_solo_sr:
            p_indicator = self.indicators[self.position - 1]
            p_hmao, p_hmacci, p_hmadir, _ = p_indicator
            hmao, hmacci, hmadir, candle = indicator

            if (hmacci > 0 and hmao > 0) or \
                    (hmacci > 0 and hmadir > 0) or \
                    (hmao > 0 and hmadir > 0) or \
                    (hmacci > 0 and hmao > 0 and hmadir > 0):
                action = SignalAction.buy
            elif (hmacci < 0 and hmao < 0) or \
                    (hmacci < 0 and hmadir < 0) or \
                    (hmao < 0 and hmadir < 0) or \
                    (hmacci < 0 and hmao < 0 and hmadir < 0):
                action = SignalAction.sell
            else:
                action = SignalAction.hold

        elif self.strategy_type is StrategyType.werewolf_basic:
            hma, candle = indicator

            back_compare = self.position - self.periods[1]
            if back_compare < 0:
                action = SignalAction.hold
            else:
                p_indicator = self.indicators[back_compare]
                p_hma, _ = p_indicator

                if p_hma < hma <= candle['close'] and candle['low'] <= hma + (hma * self.periods[2] / 100):
                    action = SignalAction.buy
                elif p_hma > hma >= candle['close'] and candle['high'] >= hma - (hma * self.periods[2] / 100):
                    action = SignalAction.sell
                else:
                    action = SignalAction.hold

        elif self.strategy_type is StrategyType.werewolf_ntwr:
            hma, candle = indicator

            back_compare = self.position - self.periods[1]
            if back_compare < 0:
                action = SignalAction.hold
            else:
                p_indicator = self.indicators[back_compare]
                p_hma, _ = p_indicator

                if self.last_not_hold_signal and self.last_not_hold_signal[0] is SignalAction.stop:
                    if p_hma < hma <= candle['close']:
                        action = SignalAction.buy
                    elif p_hma > hma >= candle['close']:
                        action = SignalAction.sell
                    else:
                        action = SignalAction.hold
                else:
                    if p_hma < hma <= candle['close'] and candle['low'] <= hma + (hma * self.periods[2] / 100):
                        action = SignalAction.buy
                    elif p_hma > hma >= candle['close'] and candle['high'] >= hma - (hma * self.periods[2] / 100):
                        action = SignalAction.sell
                    else:
                        action = SignalAction.hold

        elif self.strategy_type is StrategyType.enod_low_sma:
            sma_value, hma_value, candle = indicator

            indicator_difference = hma_value - sma_value
            if len(self.indicators) < 5:
                action = SignalAction.hold
            else:
                sma_value1, hma_value1, _ = self.indicators[self.position - 1]
                sma_value2, hma_value2, _ = self.indicators[self.position - 2]
                sma_value3, hma_value3, _ = self.indicators[self.position - 3]
                sma_value4, hma_value4, _ = self.indicators[self.position - 4]
                sma_value5, hma_value5, _ = self.indicators[self.position - 5]
                indicator_difference1 = hma_value1 - sma_value1
                indicator_difference2 = hma_value2 - sma_value2
                indicator_difference3 = hma_value3 - sma_value3
                indicator_difference4 = hma_value4 - sma_value4
                indicator_difference5 = hma_value5 - sma_value5

                if indicator_difference > indicator_difference1 > indicator_difference2 > indicator_difference3 > indicator_difference4 > indicator_difference5:
                    action = SignalAction.buy
                elif indicator_difference < indicator_difference1 < indicator_difference2 < indicator_difference3 < indicator_difference4 < indicator_difference5:
                    action = SignalAction.sell
                else:
                    action = SignalAction.hold

        elif self.strategy_type is StrategyType.enod_low_avg:
            sma_value, hma_value, candle = indicator

            if len(self.indicators) < 5:
                action = SignalAction.hold
            else:
                indicator_difference = hma_value - (sma_value + hma_value) / 2

                sma_value1, hma_value1, _ = self.indicators[self.position - 1]
                sma_value2, hma_value2, _ = self.indicators[self.position - 2]
                sma_value3, hma_value3, _ = self.indicators[self.position - 3]
                sma_value4, hma_value4, _ = self.indicators[self.position - 4]
                sma_value5, hma_value5, _ = self.indicators[self.position - 5]
                indicator_difference1 = hma_value1 - (sma_value1 + hma_value1) / 2
                indicator_difference2 = hma_value2 - (sma_value2 + hma_value2) / 2
                indicator_difference3 = hma_value3 - (sma_value3 + hma_value3) / 2
                indicator_difference4 = hma_value4 - (sma_value4 + hma_value4) / 2
                indicator_difference5 = hma_value5 - (sma_value5 + hma_value5) / 2

                if indicator_difference > indicator_difference1 > indicator_difference2 > indicator_difference3 > indicator_difference4 > indicator_difference5:
                    action = SignalAction.buy
                elif indicator_difference < indicator_difference1 < indicator_difference2 < indicator_difference3 < indicator_difference4 < indicator_difference5:
                    action = SignalAction.sell
                else:
                    action = SignalAction.hold

        elif self.strategy_type is StrategyType.enod_low_avg_simple:
            sma_value, hma_value, candle = indicator

            if len(self.indicators) < 1:
                action = SignalAction.hold
            else:
                indicator_difference = hma_value - (sma_value + hma_value) / 2

                sma_value1, hma_value1, _ = self.indicators[self.position - 1]
                indicator_difference1 = hma_value1 - (sma_value1 + hma_value1) / 2

                if indicator_difference > indicator_difference1:
                    action = SignalAction.buy
                elif indicator_difference < indicator_difference1:
                    action = SignalAction.sell
                else:
                    action = SignalAction.hold

        elif self.strategy_type is StrategyType.enod_high_sma:
            sma_value, hma_value, candle = indicator

            if len(self.indicators) < 5:
                action = SignalAction.hold
            else:
                indicator_difference = hma_value - sma_value
                cci_line = candle['close'] - sma_value

                sma_value1, hma_value1, _ = self.indicators[self.position - 1]
                sma_value2, hma_value2, _ = self.indicators[self.position - 2]
                sma_value3, hma_value3, _ = self.indicators[self.position - 3]
                sma_value4, hma_value4, _ = self.indicators[self.position - 4]
                sma_value5, hma_value5, _ = self.indicators[self.position - 5]
                indicator_difference1 = hma_value1 - sma_value1
                indicator_difference2 = hma_value2 - sma_value2
                indicator_difference3 = hma_value3 - sma_value3
                indicator_difference4 = hma_value4 - sma_value4
                indicator_difference5 = hma_value5 - sma_value5

                if cci_line > 0 and indicator_difference > indicator_difference1 > indicator_difference2 > indicator_difference3 > indicator_difference4 > indicator_difference5:
                    action = SignalAction.buy
                elif cci_line < 0 and indicator_difference < indicator_difference1 < indicator_difference2 < indicator_difference3 < indicator_difference4 < indicator_difference5:
                    action = SignalAction.sell
                else:
                    action = SignalAction.hold

        elif self.strategy_type is StrategyType.enod_high_avg:
            sma_value, hma_value, candle = indicator

            if len(self.indicators) < 5:
                action = SignalAction.hold
            else:
                indicator_difference = hma_value - (sma_value + hma_value) / 2
                cci_line = candle['close'] - sma_value

                sma_value1, hma_value1, _ = self.indicators[self.position - 1]
                sma_value2, hma_value2, _ = self.indicators[self.position - 2]
                sma_value3, hma_value3, _ = self.indicators[self.position - 3]
                sma_value4, hma_value4, _ = self.indicators[self.position - 4]
                sma_value5, hma_value5, _ = self.indicators[self.position - 5]
                indicator_difference1 = hma_value1 - (sma_value1 + hma_value1) / 2
                indicator_difference2 = hma_value2 - (sma_value2 + hma_value2) / 2
                indicator_difference3 = hma_value3 - (sma_value3 + hma_value3) / 2
                indicator_difference4 = hma_value4 - (sma_value4 + hma_value4) / 2
                indicator_difference5 = hma_value5 - (sma_value5 + hma_value5) / 2

                if cci_line > 0 and indicator_difference > indicator_difference1 > indicator_difference2 > indicator_difference3 > indicator_difference4 > indicator_difference5:
                    action = SignalAction.buy
                elif cci_line < 0 and indicator_difference < indicator_difference1 < indicator_difference2 < indicator_difference3 < indicator_difference4 < indicator_difference5:
                    action = SignalAction.sell
                else:
                    action = SignalAction.hold

        elif self.strategy_type is StrategyType.zig_zag:
            d, c, xab, xad, abc, bcd, candle = indicator

            def isBat(mode):
                _xab = xab >= 0.382 and xab <= 0.5
                _abc = abc >= 0.382 and abc <= 0.886
                _bcd = bcd >= 1.618 and bcd <= 2.618
                _xad = xad <= 0.618 and xad <= 1.000  # 0.886
                cond = d < c if mode == 'buy' else d > c
                return _xab and _abc and _bcd and _xad and cond

            def isAntiBat(mode):
                _xab = xab >= 0.500 and xab <= 0.886  # 0.618
                _abc = abc >= 1.000 and abc <= 2.618  # 1.13 --> 2.618
                _bcd = bcd >= 1.618 and bcd <= 2.618  # 2.0 --> 2.618
                _xad = xad >= 0.886 and xad <= 1.000  # 1.13
                cond = d < c if mode == 'buy' else d > c
                return _xab and _abc and _bcd and _xad and cond

            def isAltBat(mode):
                _xab = xab <= 0.382
                _abc = abc >= 0.382 and abc <= 0.886
                _bcd = bcd >= 2.0 and bcd <= 3.618
                _xad = xad <= 1.13
                cond = d < c if mode == 'buy' else d > c
                return _xab and _abc and _bcd and _xad and cond

            def isButterfly(mode):
                _xab = xab <= 0.786
                _abc = abc >= 0.382 and abc <= 0.886
                _bcd = bcd >= 1.618 and bcd <= 2.618
                _xad = xad >= 1.27 and xad <= 1.618
                cond = d < c if mode == 'buy' else d > c
                return _xab and _abc and _bcd and _xad and cond

            def isAntiButterfly(mode):
                _xab = xab >= 0.236 and xab <= 0.886  # 0.382 - 0.618
                _abc = abc >= 1.130 and abc <= 2.618  # 1.130 - 2.618
                _bcd = bcd >= 1.000 and bcd <= 1.382  # 1.27
                _xad = xad >= 0.500 and xad <= 0.886  # 0.618 - 0.786
                cond = d < c if mode == 'buy' else d > c
                return _xab and _abc and _bcd and _xad and cond

            def isABCD(mode):
                _abc = abc >= 0.382 and abc <= 0.886
                _bcd = bcd >= 1.13 and bcd <= 2.618
                cond = d < c if mode == 'buy' else d > c
                return _abc and _bcd and cond

            def isGartley(mode):
                _xab = xab >= 0.5 and xab <= 0.618  # 0.618
                _abc = abc >= 0.382 and abc <= 0.886
                _bcd = bcd >= 1.13 and bcd <= 2.618
                _xad = xad >= 0.75 and xad <= 0.875  # 0.786
                cond = d < c if mode == 'buy' else d > c
                return _xab and _abc and _bcd and _xad and cond

            def isAntiGartley(mode):
                _xab = xab >= 0.500 and xab <= 0.886  # 0.618 -> 0.786
                _abc = abc >= 1.000 and abc <= 2.618  # 1.130 -> 2.618
                _bcd = bcd >= 1.500 and bcd <= 5.000  # 1.618
                _xad = xad >= 1.000 and xad <= 5.000  # 1.272
                cond = d < c if mode == 'buy' else d > c
                return _xab and _abc and _bcd and _xad and cond

            def isCrab(mode):
                _xab = xab >= 0.500 and xab <= 0.875  # 0.886
                _abc = abc >= 0.382 and abc <= 0.886
                _bcd = bcd >= 2.000 and bcd <= 5.000  # 3.618
                _xad = xad >= 1.382 and xad <= 5.000  # 1.618
                cond = d < c if mode == 'buy' else d > c
                return _xab and _abc and _bcd and _xad and cond

            def isAntiCrab(mode):
                _xab = xab >= 0.250 and xab <= 0.500  # 0.276 -> 0.446
                _abc = abc >= 1.130 and abc <= 2.618  # 1.130 -> 2.618
                _bcd = bcd >= 1.618 and bcd <= 2.618  # 1.618 -> 2.618
                _xad = xad >= 0.500 and xad <= 0.750  # 0.618
                cond = d < c if mode == 'buy' else d > c
                return _xab and _abc and _bcd and _xad and cond

            def isShark(mode):
                _xab = xab >= 0.500 and xab <= 0.875  # 0.5 --> 0.886
                _abc = abc >= 1.130 and abc <= 1.618  #
                _bcd = bcd >= 1.270 and bcd <= 2.240  #
                _xad = xad >= 0.886 and xad <= 1.130  # 0.886 --> 1.13
                cond = d < c if mode == 'buy' else d > c
                return _xab and _abc and _bcd and _xad and cond

            def isAntiShark(mode):
                _xab = xab >= 0.382 and xab <= 0.875  # 0.446 --> 0.618
                _abc = abc >= 0.500 and abc <= 1.000  # 0.618 --> 0.886
                _bcd = bcd >= 1.250 and bcd <= 2.618  # 1.618 --> 2.618
                _xad = xad >= 0.500 and xad <= 1.250  # 1.130 --> 1.130
                cond = d < c if mode == 'buy' else d > c
                return _xab and _abc and _bcd and _xad and cond

            def is5o(mode):
                _xab = xab >= 1.13 and xab <= 1.618
                _abc = abc >= 1.618 and abc <= 2.24
                _bcd = bcd >= 0.5 and bcd <= 0.625  # 0.5
                _xad = xad >= 0.0 and xad <= 0.236  # negative?
                cond = d < c if mode == 'buy' else d > c
                return _xab and _abc and _bcd and _xad and cond

            def isWolf(mode):
                _xab = xab >= 1.27 and xab <= 1.618
                _abc = abc >= 0 and abc <= 5
                _bcd = bcd >= 1.27 and bcd <= 1.618
                _xad = xad >= 0.0 and xad <= 5
                cond = d < c if mode == 'buy' else d > c
                return _xab and _abc and _bcd and _xad and cond

            def isHnS(mode):
                _xab = xab >= 2.0 and xab <= 10
                _abc = abc >= 0.90 and abc <= 1.1
                _bcd = bcd >= 0.236 and bcd <= 0.88
                _xad = xad >= 0.90 and xad <= 1.1
                cond = d < c if mode == 'buy' else d > c
                return _xab and _abc and _bcd and _xad and cond

            def isConTria(mode):
                _xab = xab >= 0.382 and xab <= 0.618
                _abc = abc >= 0.382 and abc <= 0.618
                _bcd = bcd >= 0.382 and bcd <= 0.618
                _xad = xad >= 0.236 and xad <= 0.764
                cond = d < c if mode == 'buy' else d > c
                return _xab and _abc and _bcd and _xad and cond

            def isExpTria(mode):
                _xab = xab >= 1.236 and xab <= 1.618
                _abc = abc >= 1.000 and abc <= 1.618
                _bcd = bcd >= 1.236 and bcd <= 2.000
                _xad = xad >= 2.000 and xad <= 2.236
                cond = d < c if mode == 'buy' else d > c
                return _xab and _abc and _bcd and _xad and cond

            def f_last_fib(rate):
                fib_range = abs(d - c)
                return d - (fib_range * rate) if d > c else d + (fib_range * rate)

            target01_ew_rate = 0.236

            buy_patterns_00 = isABCD('buy') or isBat('buy') or isAltBat('buy') or isButterfly('buy') or isGartley('buy') or isCrab('buy') or isShark('buy') or is5o('buy') or isWolf('buy') or isHnS('buy') or isConTria('buy') or isExpTria('buy')
            buy_patterns_01 = isAntiBat('buy') or isAntiButterfly('buy') or isAntiGartley('buy') or isAntiCrab('buy') or isAntiShark('buy')

            sell_patterns_00 = isABCD('sell') or isBat('sell') or isAltBat('sell') or isButterfly('sell') or isGartley('sell') or isCrab('sell') or isShark('sell') or is5o('sell') or isWolf('sell') or isHnS('sell') or isConTria('sell') or isExpTria('sell')
            sell_patterns_01 = isAntiBat('sell') or isAntiButterfly('sell') or isAntiGartley('sell') or isAntiCrab('sell') or isAntiShark('sell')

            target01_buy_entry = (buy_patterns_00 or buy_patterns_01) and candle['close'] <= f_last_fib(target01_ew_rate)
            target01_sell_entry = (sell_patterns_00 or sell_patterns_01) and candle['close'] >= f_last_fib(target01_ew_rate)

            if target01_buy_entry:
                action = SignalAction.buy
            elif target01_sell_entry:
                action = SignalAction.sell
            else:
                action = SignalAction.hold

        level = 1
        if self.last_not_hold_signal is not None:
            _, _, _, _, p_level, _ = self.last_not_hold_signal
            # pyramiding logic
            if action == self.last_not_hold_signal[0]:
                level = p_level + 1

            if self.settings['pyramiding'] <= 1 and action is not SignalAction.hold and action == self.last_not_hold_signal[0]:
                action = SignalAction.hold
            elif 1 < self.settings['pyramiding'] < level:
                action = SignalAction.hold

        # reenter strategy
        if not self.settings['reenter'] and self.pre_last_not_hold_signal is not None and self.last_not_hold_signal is not None:
            if self.pre_last_not_hold_signal[0] is SignalAction.buy and self.last_not_hold_signal[0] is SignalAction.stop and action is SignalAction.buy:
                action = SignalAction.hold
            if self.pre_last_not_hold_signal[0] is SignalAction.sell and self.last_not_hold_signal[0] is SignalAction.stop and action is SignalAction.sell:
                action = SignalAction.hold

        if action in [SignalAction.buy, SignalAction.sell] and (self.last_opposite_signal is None or (action is self.last_opposite_signal[0])):
            self.last_opposite_signal = self.last_not_hold_signal

        # calc profit for signal
        profit = 0
        if self.last_not_hold_signal and self.last_not_hold_signal[0] is not SignalAction.stop:
            _, p_price, _, _, _, _ = self.last_not_hold_signal
            if action is SignalAction.buy:
                profit = ((p_price - candle['close']) / p_price) * 100
            elif action is SignalAction.sell:
                profit = ((candle['close'] - p_price) / p_price) * 100

        signal = (action, candle['close'], candle['open_date_dt'] + datetime.timedelta(minutes=self.timeframe), indicator, level, profit)
        return signal
