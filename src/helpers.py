import datetime
import itertools
import logging
import signal
from threading import current_thread

import pytz
from future.utils import string_types
from sqlalchemy.exc import SQLAlchemyError, IntegrityError

from src.models import DBSession, sessionmaker, BitfinexCandle, BinanceCandle

logger = logging.getLogger(__name__)
K = 1000


def grouper(n, iterable, fillvalue=None):
    """
    Split list to n-lengths groups
    :param n: Length of group
    :param iterable: list
    :param fillvalue: value to fill None values
    :return: list of groups
    """
    args = [iter(iterable)] * n
    return list(map(list, itertools.zip_longest(*args, fillvalue=fillvalue)))


def datetime_to_minutes(dt):
    return dt.hour * 60 + dt.minute


def minute_to_timedelta(m):
    hours = m // 60
    minutes = m % 60
    days = 0
    if hours > 24:
        days = hours // 24
        hours = hours % 24

    return datetime.timedelta(days=days, hours=hours, minutes=minutes)


def get_compatible_timeframe(timeframe):
    """
    Get timeframe for api request
    :param timeframe:
    :return: (timeframe string, number of candles)
    """
    available_timeframes = [1, 5, 15, 30, 60, 180, 360, 720, 1440, 10080, 20160]

    prev = 1
    for tf in available_timeframes:
        if timeframe % tf == 0:
            prev = tf

    return prev


def convert_timeframe_to_string(timeframe):
    if timeframe < 60:
        return f'{timeframe}m'
    elif 60 <= timeframe < 1440:
        return f'{timeframe // 60}h'
    elif timeframe >= 1440 <= 20160:
        return f'{timeframe // 1440}D'
    else:
        raise ValueError("TimeFrame too high")


_signames = {v: k
             for k, v in reversed(sorted(vars(signal).items()))
             if k.startswith('SIG') and not k.startswith('SIG_')}


def get_signal_name(signum):
    """Returns the signal name of the given signal number."""
    return _signames[signum]


def add_to_db(objects, session=None):
    if not objects:
        return True

    if not isinstance(objects, list):
        objects = [objects]

    if session is None:
        session = sessionmaker.object_session(objects[0])
        if session is None:
            session = DBSession

    try:
        for object in objects:
            session.add(object)
        session.commit()
        return True
    except (SQLAlchemyError, IntegrityError) as e:
        session.rollback()
        logger.critical(f"Database error: {str(e)}")
        return False


def get_enum(value, enum_class, var_name):
    if isinstance(value, enum_class):
        return value
    elif isinstance(value, string_types):
        try:
            return enum_class[value]
        except KeyError:
            raise TypeError(f"{var_name} must be str or {enum_class.__name__}")
    else:
        raise TypeError(f"{var_name} must be str or {enum_class.__name__}")


def thread_wrapper(target, *args, **kwargs):
    thr_name = current_thread().name
    logger.debug('{0} - started'.format(thr_name))
    try:
        target(*args, **kwargs)
    except Exception:
        logger.exception('unhandled exception in %s', thr_name)
        raise
    logger.debug('{0} - ended'.format(thr_name))


def combine_candle(candle_group, timeframe):
    candle_date_minutes = datetime_to_minutes(candle_group[0].open_date_dt)
    open_date = datetime.datetime.utcfromtimestamp((candle_group[0].open_date - (candle_date_minutes * 60000) + ((candle_date_minutes - candle_date_minutes % timeframe) * 60000)) // K).replace(tzinfo=pytz.utc)
    close_date = datetime.datetime.utcfromtimestamp((candle_group[-1].close_date - (candle_date_minutes * 60000) + ((candle_date_minutes - candle_date_minutes % timeframe) * 60000)) // K).replace(tzinfo=pytz.utc)

    new_candle = BinanceCandle(
        open=candle_group[0].open,
        close=candle_group[-1].close,
        high=max([candle.high for candle in candle_group]),
        low=min([candle.low for candle in candle_group]),
        volume=sum([candle.volume for candle in candle_group]),
        quote_asset_volume=sum([candle.quote_asset_volume for candle in candle_group]),
        ticker=candle_group[0].ticker,
        open_date=int(open_date.timestamp() * K),
        close_date=int(close_date.timestamp() * K),
        trades=sum([candle.trades for candle in candle_group]),
    )
    return new_candle


def combine_dict_candle(candle_group, timeframe):
    candle_date_minutes = datetime_to_minutes(candle_group[0]['open_date_dt'])
    open_date = datetime.datetime.utcfromtimestamp((candle_group[0]['open_date'] - (candle_date_minutes * 60000) + ((candle_date_minutes - candle_date_minutes % timeframe) * 60000)) // K).replace(tzinfo=pytz.utc)

    new_candle = {
        "open": candle_group[0]['open'],
        "close": candle_group[-1]['close'],
        "high": max([candle['high'] for candle in candle_group]),
        "low": min([candle['low'] for candle in candle_group]),
        "volume": sum([candle['volume'] for candle in candle_group]),
        "ticker": candle_group[0]['ticker'],
        "open_date": int(open_date.timestamp() * K),
        "open_date_dt": datetime.datetime.utcfromtimestamp(int(open_date.timestamp())).replace(tzinfo=pytz.utc),
    }

    return new_candle
