import datetime
import logging
import time

import pytz
import requests
from bitfinex import ClientV2
from bitfinex.rest.restv2 import BitfinexException

from src.helpers import datetime_to_minutes, minute_to_timedelta
from src.models import BitfinexCandle, Session
from src.settings import SLEEP_AFTER_BITFINEX_REQUEST

logger = logging.getLogger(__name__)

K = 1000


class CandleRequester:
    request_start_time = time.time()
    request_end_time = time.time()

    def __init__(self, ticker, start_date, end_date):
        self.ticker = ticker
        self.start_date = start_date
        self.end_date = end_date
        self.session = Session()

    def actual_start_date(self):
        first_candle = self.session.query(BitfinexCandle) \
            .filter(BitfinexCandle.open_date <= round(self.end_date.timestamp() * K)) \
            .filter(BitfinexCandle.open_date >= round(self.start_date.timestamp() * K)) \
            .order_by(BitfinexCandle.open_date.desc()).first()

        if first_candle:
            return first_candle.open_date_dt
        else:
            return self.start_date

    def actual_end_date(self):
        actual_start_date = self.actual_start_date()

        last_candle = self.session.query(BitfinexCandle) \
            .filter(BitfinexCandle.open_date <= round(self.end_date.timestamp() * K)) \
            .filter(BitfinexCandle.open_date > round(actual_start_date.timestamp() * K)) \
            .order_by(BitfinexCandle.open_date.asc()).first()

        if last_candle:
            return last_candle.open_date_dt
        else:
            return self.end_date

    def request_candles(self):
        dates = []

        request_start_date = self.start_date

        while request_start_date < self.end_date:
            request_end_date = request_start_date + datetime.timedelta(minutes=1000)

            first_candle = self.session.query(BitfinexCandle) \
                .filter(BitfinexCandle.open_date <= round(request_end_date.timestamp() * K)) \
                .filter(BitfinexCandle.open_date > round(request_start_date.timestamp() * K)) \
                .order_by(BitfinexCandle.open_date.asc()).first()

            last_candle = self.session.query(BitfinexCandle) \
                .filter(BitfinexCandle.open_date <= round(request_end_date.timestamp() * K)) \
                .filter(BitfinexCandle.open_date > round(request_start_date.timestamp() * K)) \
                .order_by(BitfinexCandle.open_date.desc()).first()

            if first_candle and first_candle.open_date < round(request_end_date.timestamp() * K) and first_candle.open_date > round(request_start_date.timestamp() * K):
                ed = first_candle.open_date_dt
            else:
                ed = request_end_date

            if last_candle and last_candle.open_date > round(request_start_date.timestamp() * K) and last_candle.open_date < round(request_end_date.timestamp() * K):
                sd = last_candle.open_date_dt
            else:
                sd = request_start_date

            dates.append((sd, ed))
            request_start_date = request_end_date

        for date in dates:
            self.request_candles_group(*date)

    def request_candles_group(self, start_date, end_date):
        client = ClientV2()

        logger.debug(f"Requesting missing candles for {self.ticker}")
        our_tz = pytz.timezone('Europe/Kiev')

        request_start_date = start_date.replace(tzinfo=our_tz)
        request_end_date = end_date.replace(tzinfo=our_tz)

        last_request_start_date = request_start_date

        requests_finished = 0
        failed_requests = 0
        while last_request_start_date < request_end_date:
            try:
                CandleRequester.request_start_time = time.time()
                time_from_last_request = CandleRequester.request_start_time - CandleFormer.request_end_time
                if time_from_last_request < SLEEP_AFTER_BITFINEX_REQUEST:
                    time.sleep(SLEEP_AFTER_BITFINEX_REQUEST - time_from_last_request)

                raw_candles = client.candles('1m',
                                             f"t{self.ticker}",
                                             'hist',
                                             limit=1000,
                                             start=int(last_request_start_date.timestamp() * K),
                                             sort=1)
                missed_candles = int((BitfinexCandle.from_list(raw_candles[-1]).open_date - BitfinexCandle.from_list(raw_candles[0]).open_date) // 60000) - 1000
                missed_candles = missed_candles if missed_candles > 0 else 0

                logger.info("Requested candles for period {frm} - {to} for {ticker}, missed {missed} candles"
                            .format(frm=BitfinexCandle.from_list(raw_candles[0]).open_date_dt.strftime('%d.%m.%Y %H:%M'),
                                    to=BitfinexCandle.from_list(raw_candles[-1]).open_date_dt.strftime('%d.%m.%Y %H:%M'),
                                    ticker=self.ticker,
                                    missed=missed_candles))

                requests_finished += 1

                if not raw_candles:
                    break

                candles_in_database = self.session.query(BitfinexCandle.open_date, BitfinexCandle.ticker) \
                    .filter(BitfinexCandle.open_date >= BitfinexCandle.from_list(raw_candles[0], ticker=self.ticker).open_date) \
                    .filter(BitfinexCandle.open_date <= BitfinexCandle.from_list(raw_candles[-1], ticker=self.ticker).open_date) \
                    .filter(BitfinexCandle.ticker == self.ticker).all()

                for rc in raw_candles:
                    candle = BitfinexCandle.from_list(rc, ticker=self.ticker)
                    if (candle.open_date, candle.ticker) not in candles_in_database:
                        candle.add_or_update(self.session)

                    last_request_start_date = candle.open_date_dt
                self.session.commit()
                CandleFormer.request_end_time = time.time()

            except BitfinexException:
                logger.warning(f"Ratelimit exceeded, retrying after 30 seconds")
                failed_requests += 1
                time.sleep(30)
            except (requests.exceptions.ConnectionError, requests.exceptions.ReadTimeout):
                logger.warning(f"Bitfinex didn't respond in time, retrying after 10 seconds")
                failed_requests += 1
                time.sleep(10)

        logger.debug(f"Finished with {requests_finished} requests. and {failed_requests} failed requests.")


class CandleFormer:
    """Requests candles from database and forms timeframes

    Args:
        timeframe(:obj:`int`) Timeframe to form.
        period(:obj:`int`) How many candles need to request.
        ticker(:obj:`str`) Pair to request. Ex. 'BTCUSD'
    """

    request_start_time = time.time()
    request_end_time = time.time()

    def __init__(self, ticker, timeframe, start_date, end_date):
        self.ticker = ticker
        self.timeframe = timeframe
        self.start_date = start_date
        self.end_date = end_date
        self.session = Session()

    def query(self):
        logger.debug(f"Forming candles from {self.start_date.strftime('%d.%m.%Y %H:%M')} to {self.end_date.strftime('%d.%m.%Y %H:%M')}")
        candles = self.session.query(BitfinexCandle) \
            .filter(BitfinexCandle.ticker == self.ticker) \
            .filter(BitfinexCandle.open_date < round(self.end_date.replace(tzinfo=pytz.utc).timestamp() * K)) \
            .filter(BitfinexCandle.open_date >= round(self.start_date.replace(tzinfo=pytz.utc).timestamp() * K)) \
            .order_by(BitfinexCandle.open_date.desc()).all()
        self.session.expunge_all()
        return candles

    def get_candles(self):
        """Gives candles grouped with self.timeframe
        :return: list of BitfinexCandle
        """
        candles = self.combine_candles(self.grouped_candles())

        return list(reversed(candles))

    def combine_candles(self, grouped_raw_candles):
        """Make self.timeframe candles from groups of one minute candles.

        :param grouped_raw_candles: list[list[BitfinexCandle]]
        :return: list[BitfinexCandle]
        """

        if self.timeframe <= 1:
            return [candle_group[0] for candle_group in grouped_raw_candles]
        else:
            candle_list = []
            for candle_group in grouped_raw_candles:
                candle_date_minutes = datetime_to_minutes(candle_group[0].open_date_dt)
                open_date = datetime.datetime.utcfromtimestamp((candle_group[0].open_date - (candle_date_minutes * 60000) + ((candle_date_minutes - candle_date_minutes % self.timeframe) * 60000)) // K).replace(tzinfo=pytz.utc)

                new_candle = BitfinexCandle(
                    open=candle_group[-1].open,
                    close=candle_group[0].close,
                    high=max([candle.high for candle in candle_group]),
                    low=min([candle.low for candle in candle_group]),
                    volume=sum([candle.volume for candle in candle_group]),
                    ticker=self.ticker,
                    open_date=int(open_date.timestamp() * K),
                )
                candle_list.append(new_candle)

            return candle_list

    def grouped_candles(self):
        """Groups one minute candles to self.timeframe sized groups.

        :return: list[list[BitfinexCandles]]
        """
        raw_candles = list(reversed(self.query()))

        start_time = datetime.datetime.utcfromtimestamp(raw_candles[0].open_date / K).replace(tzinfo=pytz.utc)
        end_time = start_time + minute_to_timedelta(self.timeframe)
        if start_time.day != end_time.day:
            end_time = end_time.replace(hour=0, minute=0, second=0, microsecond=0)

        candle_group = []
        list_of_groups = []
        for candle in raw_candles:
            if start_time <= candle.open_date_dt < end_time:
                candle_group.append(candle)
            else:
                list_of_groups.append(list(reversed(candle_group)))
                candle_group = [candle]
                start_time = end_time
                end_time = end_time + minute_to_timedelta(self.timeframe)
                if start_time.day != end_time.day:
                    end_time = end_time.replace(hour=0, minute=0, second=0, microsecond=0)
        list_of_groups.append(list(reversed(candle_group)))

        return reversed(list_of_groups)
