import datetime
import enum
import logging

import pytz
from sqlalchemy import create_engine, Column, Integer, String, Float, UniqueConstraint, BigInteger
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

from src.settings import DATABASE

logger = logging.getLogger(__name__)

Base = declarative_base()
engine = create_engine(DATABASE)


class SignalAction(enum.Enum):
    buy = 'buy'
    sell = 'sell'
    hold = 'hold'
    stop = 'stop'


class StrategyType(enum.Enum):
    ao = 'ao'
    ma2oc = 'ma2oc'
    han_solo_jr = 'han_solo_jr'
    han_solo_sr = 'han_solo_sr'
    han_solo_3 = 'han_solo_3'
    werewolf_basic = 'werewolf_basic'
    werewolf_ntwr = 'werewolf_ntwr'
    enod_low_sma = "enod_low_sma"
    enod_low_avg = "enod_low_avg"
    enod_low_avg_simple = "enod_low_avg_simple"
    enod_high_sma = "enod_high_sma"
    enod_high_avg = "enod_high_avg"
    zig_zag = "zig_zag"

    def get_name(self):
        if self == StrategyType.ao:
            return "AO"
        elif self == StrategyType.ma2oc:
            return "MA2OC"
        elif self == StrategyType.han_solo_jr:
            return "Han Solo JR"
        elif self == StrategyType.han_solo_sr:
            return "Han Solo SR"
        elif self == StrategyType.han_solo_3:
            return "Han Solo 3"
        elif self == StrategyType.werewolf_basic:
            return "Werewolf basic"
        elif self == StrategyType.werewolf_ntwr:
            return "Werewolf no traceback when reenter"
        elif self == StrategyType.enod_low_sma:
            return "Enod Low TF HMA-SMA"
        elif self == StrategyType.enod_low_avg:
            return "Enod Low TF HMA-AVG"
        elif self == StrategyType.enod_high_sma:
            return "Enod High TF HMA-SMA"
        elif self == StrategyType.enod_high_avg:
            return "Enod High TF HMA-AVG"
        elif self == StrategyType.enod_low_avg_simple:
            return "Enod High TF HMA-AVG simple"
        elif self == StrategyType.zig_zag:
            return "Zig zag"
        else:
            return "Unknown"


class PositionType(enum.Enum):
    sell = 'sell'
    buy = 'buy'


class BitfinexCandle(Base):
    __tablename__ = 'bitfinex_candles'

    id = Column(Integer, primary_key=True)
    open_date = Column(BigInteger, nullable=False)
    open = Column(Float, nullable=False)
    close = Column(Float, nullable=False)
    high = Column(Float, nullable=False)
    low = Column(Float, nullable=False)
    volume = Column(Float, nullable=False)
    ticker = Column(String, nullable=False)
    __table_args__ = (UniqueConstraint('open_date', 'ticker', name='_opendate_ticker_uc'),)

    def to_dict(self):
        return {
            'open': self.open,
            'close': self.close,
            'high': self.high,
            'low': self.low,
            'open_date': self.open_date,
            'volume': self.volume,
            'ticker': self.ticker
        }

    @property
    def open_date_dt(self):
        date = datetime.datetime.utcfromtimestamp(self.open_date / 1e3)
        return date.replace(tzinfo=pytz.utc)

    @classmethod
    def from_list(cls, candle, ticker=None):
        return BitfinexCandle(open=candle[1],
                              close=candle[2],
                              high=candle[3],
                              low=candle[4],
                              volume=candle[5],
                              open_date=candle[0],
                              ticker=ticker)

    def add_or_update(self, session=None):
        if not session:
            session = DBSession

        candle_obj = session.query(BitfinexCandle).filter(BitfinexCandle.open_date == self.open_date).filter(BitfinexCandle.ticker == self.ticker).first()
        if candle_obj:
            candle_obj.open = self.open
            candle_obj.close = self.close
            candle_obj.high = self.high
            candle_obj.low = self.low
            candle_obj.volume = self.volume
            candle_obj.ticker = self.ticker
            session.add(candle_obj)
        else:
            session.add(self)

    @classmethod
    def has_candle(cls, open_date, ticker, session=None):
        if session is None:
            session = DBSession
        return session.query(session.query(BitfinexCandle).filter(BitfinexCandle.open_date == open_date).filter(BitfinexCandle.ticker == ticker).exists()).scalar()

    def __repr__(self):
        return "Candle(open={open}, close={close}, high={high}, low={low}, open_date={open_date}, volume={volume})" \
            .format(open=self.open, close=self.close, high=self.high, low=self.low, open_date=self.open_date_dt.strftime("%d.%m.%Y %H:%M:%S"), volume=self.volume)


class BinanceCandle(Base):
    __tablename__ = 'binance_candles'

    id = Column(Integer, primary_key=True)
    open_date = Column(BigInteger, nullable=False)
    close_date = Column(BigInteger, nullable=False)
    open = Column(Float, nullable=False)
    close = Column(Float, nullable=False)
    high = Column(Float, nullable=False)
    low = Column(Float, nullable=False)
    volume = Column(Float, nullable=False)
    quote_asset_volume = Column(Float, nullable=False)
    trades = Column(Integer, nullable=False)
    ticker = Column(String, nullable=False)
    __table_args__ = (UniqueConstraint('open_date', 'ticker', name='_bncc_opendate_ticker_uc'),)

    def to_dict(self):
        return {
            'open': self.open,
            'close': self.close,
            'high': self.high,
            'low': self.low,
            'open_date': self.open_date,
            'open_date_dt': self.open_date_dt,
            'volume': self.volume,
            'ticker': self.ticker
        }

    @property
    def open_date_dt(self):
        date = datetime.datetime.utcfromtimestamp(self.open_date / 1e3)
        return date.replace(tzinfo=pytz.utc)

    @property
    def close_date_dt(self):
        date = datetime.datetime.utcfromtimestamp(self.close_date / 1e3)
        return date.replace(tzinfo=pytz.utc)

    @classmethod
    def from_list(cls, candle, ticker=None):
        return BinanceCandle(open_date=candle[0],
                             open=candle[1],
                             high=candle[2],
                             low=candle[3],
                             close=candle[4],
                             volume=candle[5],
                             close_date=candle[6],
                             quote_asset_volume=candle[7],
                             trades=candle[8],
                             ticker=ticker)

    @classmethod
    def from_dict(cls, candle, ticker=None):
        return BinanceCandle(open_date=candle['t'],
                             close_date=candle['T'],
                             open=candle['o'],
                             close=candle['c'],
                             high=candle['h'],
                             low=candle['l'],
                             volume=candle['v'],
                             quote_asset_volume=candle['q'],
                             trades=candle['n'],
                             ticker=ticker)

    def add_or_update(self, session=None):
        if not session:
            session = DBSession

        candle_obj = session.query(BinanceCandle).filter(BinanceCandle.open_date == self.open_date).filter(BinanceCandle.ticker == self.ticker).first()
        if candle_obj:
            candle_obj.open = self.open
            candle_obj.close = self.close
            candle_obj.high = self.high
            candle_obj.low = self.low
            candle_obj.volume = self.volume
            candle_obj.quote_asset_volume = self.quote_asset_volume
            candle_obj.trades = self.trades
            candle_obj.ticker = self.ticker
            session.add(candle_obj)
        else:
            session.add(self)

    @classmethod
    def has_candle(cls, open_date, ticker, session=None):
        if session is None:
            session = DBSession
        return session.query(session.query(BinanceCandle).filter(BinanceCandle.open_date == open_date).filter(BinanceCandle.ticker == ticker).exists()).scalar()

    def __repr__(self):
        return "Candle(open={open}, close={close}, high={high}, low={low}, open_date={open_date}, volume={volume}, quote_asset_volume={quote_asset_volume}, close_date={close_date}, trades={trades})" \
            .format(open=self.open,
                    close=self.close,
                    high=self.high,
                    low=self.low,
                    open_date=self.open_date_dt.strftime("%d.%m.%Y %H:%M:%S"),
                    volume=self.volume,
                    quote_asset_volume=self.quote_asset_volume,
                    close_date=self.close_date_dt.strftime("%d.%m.%Y %H:%M:%S"),
                    trades=self.trades)


class Signal:
    def __init__(self, indicator_values, action, create_date):
        self.indicator_value = indicator_values
        self.action = action
        self.create_date = create_date


class Strategy:
    def __init__(self, ticker, indicator, periods, timeframe, deposit=0, fee=0.2, pyramiding=1):
        self.fee = fee
        self.deposit = deposit
        self.ticker = ticker
        self.indicator = indicator
        self.periods = periods
        self.timeframe = timeframe
        self.pyramiding = pyramiding
        self.signals = []
        self.active_position = None
        self.positions = []

    def update_position(self, position):
        if self.active_position is not None:
            self.positions.append(self.active_position)
        self.active_position = position

    @property
    def name(self):
        if self.periods and self.indicator and self.periods and self.timeframe:
            periods_s = [str(x) for x in self.periods]
            name = f"{self.indicator} {'/'.join(periods_s)} {(str(self.timeframe // 60) + 'h' if self.timeframe >= 60 and self.timeframe % 60 == 0 else str(self.timeframe) + 'm')}"
            return name
        else:
            return "Unknown"


class Position:
    def __init__(self, type, amount, price, level=1, create_date=None):
        self.type = type
        self.amount = amount
        self.price = price
        self.level = level
        self.create_date = datetime.datetime.utcnow() if create_date is None else create_date


Session = sessionmaker(bind=engine)
DBSession = Session()

Base.metadata.create_all(engine)
