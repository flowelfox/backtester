import argparse
import datetime
import time
import traceback
from csv import DictWriter
from os import path

import numpy as np
import psutil
import ray
from matplotlib import pyplot as plt
from termcolor import cprint

from src.candles.binance.candle_former import CandleRequester as BNCCandleRequester, CandleFormer as BNCCandleFormer
from src.candles.bitfinex.candle_former import CandleRequester as BFXCandleRequester, CandleFormer as BFXCandleFormer
from src.models import StrategyType, SignalAction
from src.settings import SETTINGS, PROJECT_ROOT
from src.signal_controller import SignalFormer

print(f"{datetime.datetime.now().strftime('%d.%m.%Y %H:%M')} Initializing ray...")
num_cpus = psutil.cpu_count(logical=False)
ray.init(num_cpus=num_cpus)


class Computer:

    def __init__(self, settings):
        self.settings = settings

    def compute_one_cycle(self, data):
        try:
            candles_id, periods = data
            candles = ray.get(candles_id)
            sf = SignalFormer(candles, periods, self.settings)
            signals = sf.generate_signals()

            if SETTINGS['save_signals']:
                self.save_signals(signals, periods)

            computed_data = self.post_computing(signals, periods)
            computed_data.update(period="|".join([str(p) for p in periods]))
            return computed_data
        except Exception as e:
            print(f"Error while computing on {periods}: {str(e)}")
            traceback.print_exc()

    def save_signals(self, signals, periods, prefix=""):
        periods_str = '|'.join([str(p) for p in periods])
        with open(path.join(PROJECT_ROOT, "resources", f"{prefix}signals_{'_'.join([str(p) for p in periods])}_{datetime.datetime.now().time().strftime('%H.%M')}.csv"), "w") as file:
            columns = ["periods", "action", "price", "date", "pyramiding", "profit"]
            writer = DictWriter(file, columns)
            writer.writeheader()

            for signal in signals:
                action, price, date, indicator, pyramiding, profit = signal

                if action is SignalAction.buy:
                    cprint(f"  {action.value.upper()} ", 'red', "on_green", ['bold'], end=" ")
                if action is SignalAction.sell:
                    cprint(f" {action.value.upper()} ", 'green', "on_red", ['bold'], end=" ")
                if action is SignalAction.hold:
                    cprint(f" {action.value.upper()} ", 'blue', "on_grey", ['bold'], end=" ")
                if action is SignalAction.stop:
                    cprint(f" {action.value.upper()} ", 'magenta', "on_grey", ['bold'], end=" ")

                print(f"{periods_str} ", indicator, price, date, pyramiding, profit)

                csv_data = {
                    "periods": periods_str,
                    "action": action.value.upper(),
                    "price": price,
                    "date": date,
                    "pyramiding": pyramiding,
                    "profit": profit,
                }
                writer.writerow(csv_data)
            print("========================================")

    def post_computing(self, signals, periods):
        if not signals:
            return {
                "net profit": 0,
                # "deposit": round(deposit, 2),
                "max drawdown": 0,
                "gross profit": 0,
                "gross loss": 0,
                "profit factor": 0,
                "fee paid": 0,
                "total trades": 0,
                "avg winning trade": 0,
                "avg loss trade": 0,
                "% profitable": 0,
                "avg trade": 0,
            }

        start_deposit = SETTINGS['start_deposit']
        max_deposit = start_deposit
        commission = SETTINGS['commission']
        gross_profit_deposit = start_deposit
        gross_loss_deposit = start_deposit
        fee_paid = 0
        profit_signals = 0
        loss_signals = 0
        stop_signals = 0

        yield_curve = []
        frozen_deposit_curve = []
        deposit = SETTINGS['start_deposit']
        frozen_deposit = 0
        position = []
        p_position = []
        grouped_signals = []
        for idx, signal in enumerate(signals):
            action, _, date, indicator, level, _ = signal
            position.append({"action": signal[0],
                             "close": signal[1],
                             "date": signal[2],
                             "indicator": signal[3],
                             "level": signal[4],
                             "profit": signal[5]})

            if (idx + 1) < len(signals):
                n_signal = signals[idx + 1]
                if n_signal[0] == action:
                    continue

            if p_position:
                if p_position[0]['action'] is SignalAction.stop:
                    # previous signal was stop then we have 0 profit for now
                    profit = 0
                else:
                    p_price = sum([s['close'] for s in p_position]) / len(p_position)

                    if action is SignalAction.buy:
                        profit = ((p_price - position[0]['close']) / p_price) * 100 * (p_position[-1]['level'] / SETTINGS['pyramiding'])
                    elif action is SignalAction.sell:
                        profit = ((position[0]['close'] - p_price) / p_price) * 100 * (p_position[-1]['level'] / SETTINGS['pyramiding'])
                    elif action is SignalAction.stop:
                        if p_position[0]['action'] is SignalAction.buy:
                            profit = ((position[0]['close'] - p_price) / p_price) * 100 * (p_position[-1]['level'] / SETTINGS['pyramiding'])
                        elif p_position[0]['action'] is SignalAction.sell:
                            profit = ((p_price - position[0]['close']) / p_price) * 100 * (p_position[-1]['level'] / SETTINGS['pyramiding'])

            else:
                profit = 0

            signal_group_price = sum([s['close'] for s in position]) / len(position)
            grouped_signals.append((action, signal_group_price, position[0]['date'], position[0]['indicator'], position[0]['level'], profit))
            yield_curve.append(deposit)
            frozen_deposit_curve.append(frozen_deposit)
            if commission:
                if not p_position or action is SignalAction.stop:
                    profit = profit - commission
                    fee_paid += deposit * (commission / 100)
                else:
                    if p_position[0]['action'] is SignalAction.stop:
                        # if previous signal was stop, then add only one commission
                        profit = profit - commission
                        fee_paid += deposit * (commission / 100)
                    else:
                        # if positions reversed, than add 2 comissions
                        profit = profit - (commission * 2)
                        fee_paid += deposit * ((commission * 2) / 100)

            delta_deposit = deposit * (profit / 100)

            # save deposit to frozen deposit if it's possible
            if delta_deposit > 0 and (deposit + delta_deposit / 2) >= max_deposit:
                deposit += delta_deposit / 2
                frozen_deposit += delta_deposit / 2
            else:
                deposit += delta_deposit

            # update max deposit
            if deposit > max_deposit:
                max_deposit = deposit

            # update gross loss and gross profit deposits
            if delta_deposit > 0:
                gross_profit_deposit += (delta_deposit / 2) if deposit >= max_deposit else delta_deposit
                profit_signals += 1
            elif delta_deposit < 0:
                gross_loss_deposit += delta_deposit
                loss_signals += 1

            if action is SignalAction.stop:
                stop_signals += 1

            p_position = position.copy()
            position.clear()

        if SETTINGS['save_signals_grouped']:
            self.save_signals(grouped_signals, periods, "GROUPED_")
        yield_curve.append(deposit)
        frozen_deposit_curve.append(frozen_deposit)
        net_profit = (deposit - start_deposit) / start_deposit * 100
        gross_profit = (gross_profit_deposit - start_deposit) / start_deposit * 100
        gross_loss = (gross_loss_deposit - start_deposit) / start_deposit * 100

        # draw yield curve
        try:
            xs = np.array(yield_curve)
            i = np.argmax(np.maximum.accumulate(xs) - xs)  # end of the period
            j = np.argmax(xs[:i])  # start of period
            max_drawdown = (xs[i] - xs[j]) / xs[j] * 100

            if SETTINGS['draw_plot']:
                plt.plot(xs)
                plt.plot([i, j], [xs[i], xs[j]], 'o', color='Red', markersize=5)
        except ValueError:
            max_drawdown = 0

        # draw frozen deposit curve
        xs = np.array(frozen_deposit_curve)

        if SETTINGS['draw_plot']:
            plt.plot(xs, color="Blue")
            plt.show()

        profit_factor = gross_profit / abs(gross_loss)

        fee_paid -= start_deposit * (commission / 100)
        fee_paid -= deposit * (commission / 100)

        total_trades = len(signals)

        average_winning_trade = gross_profit / profit_signals if profit_signals else 0
        average_loss_trade = gross_loss / loss_signals

        percent_profitable = profit_signals / total_trades * 100 if total_trades else 0

        average_trade = net_profit / total_trades if total_trades else 0

        result_dict = {
            "net profit": round(net_profit, 2),
            # "deposit": round(deposit, 2),
            "frozen deposit": round(frozen_deposit, 2),
            "max drawdown": round(max_drawdown, 2),
            "gross profit": round(gross_profit, 2),
            "gross loss": round(gross_loss, 2),
            "profit factor": round(profit_factor, 2),
            "fee paid": round(fee_paid, 2),
            "total trades": round(total_trades, 2),
            "avg winning trade": round(average_winning_trade, 2),
            "avg loss trade": round(average_loss_trade, 2),
            "% profitable": round(percent_profitable, 2),
            "avg trade": round(average_trade, 2),
        }

        return result_dict


@ray.remote(num_cpus=1)
class RayComputer(Computer):
    pass


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Backtest settings', add_help=True)
    parser.add_argument('-s', '--symbol', dest="symbol", type=str, help='left ticker and right ticker', required=True)
    parser.add_argument('-m', '--tf', dest="timeframe", type=int, help='timeframe in minutes', required=True)
    parser.add_argument('--atf', dest="alt_timeframe", type=int, help='alt timeframe in minutes', required=True)
    parser.add_argument('-st', '--strategy', dest="strategy_type", type=str, help='Strategy type', required=True, choices=['ao', 'han_solo_3', 'ma2oc', 'han_solo_jr', 'han_solo_sr', 'werewolf_basic', 'werewolf_ntwr', 'enod_low_sma', 'enod_low_avg', 'enod_high_sma', 'enod_high_avg', 'enod_low_avg_simple', 'zig_zag'])
    parser.add_argument('-fp', '--from_periods', dest="start_periods", type=int, nargs='+', help='From indicator periods', required=True)
    parser.add_argument('-tp', '--to_periods', dest="end_periods", type=int, nargs='+', help='To indicator periods', required=True)
    parser.add_argument('-f', '--from', dest="start_time", type=str, help='Time segment to test. From DD.MM.YYYY HH:MM (hours and minutes should be multiple of timeframe)', required=True)
    parser.add_argument('-t', '--to', dest="end_time", type=str, help='Time segment to test. To DD.MM.YYYY')
    # PROPERTIES
    parser.add_argument('-d', '--deposit', dest="deposit", type=int, default=100000, help='Initial capital, $ (deposit)')
    parser.add_argument('-sp', '--trailing_stop_perc', dest="trailing_stop_perc", type=float, default=None, help='Trailing stop percent')
    parser.add_argument('-mp', '--min_profit_for_stop', dest="min_profit_for_stop", type=float, default=0, help='Minimal profit percent to move stop price')
    parser.add_argument('-pd', '--profit_price_divider', dest="profit_price_divider", type=float, default=0, help='Profit price divider for stop price')
    parser.add_argument('-c', '--commission', dest="commission", type=float, help='Commission, %%', default=0.2)
    parser.add_argument('-o', '--output', dest="output", type=str, help='Output file', default=str(path.join(PROJECT_ROOT, "resources", "result.csv")))
    parser.add_argument('-p', '--provider', dest="provider", type=str, help='Candles provider', default="BITFINEX", choices=['BITFINEX', 'BINANCE'])
    parser.add_argument('-u', '--update', dest="update", action="store_true", help='Update candles', default=False)
    parser.add_argument('-ss', '--save_signals', dest="save_signals", action="store_true", help='Save and print signals', default=False)
    parser.add_argument('-ssg', '--save_signals_grouped', dest="save_signals_grouped", action="store_true", help='Save and print grouped signals', default=False)
    parser.add_argument('-dp', '--draw_plot', dest="draw_plot", action="store_true", help='Draw plot', default=False)
    parser.add_argument('-re', '--reenter', dest="reenter", action="store_true", help='Reenter to position after stop', default=False)
    parser.add_argument('-py', '--pyramiding', dest="pyramiding", type=int, help='Pyramiding for signals', default=1)
    parser.add_argument('-sv', '--stop_variant', dest="stop_variant", type=str, help='Stop variant', default="NORMAL", choices=['NORMAL', 'MAX', 'AVG', 'STAIRS_MAX', "STAIRS_AVG"])
    parser.add_argument('-pl', '--processes_limit', dest="processes_limit", type=int, help='How many processes to use. More candles = more RAM per process. Do not set bigger than CPU Cores count.', default=1)

    # args = parser.parse_args()
    args, unknown = parser.parse_known_args()
    output = args.output

    if not isinstance(args.start_periods, list):
        start_periods = [args.start_periods]
    else:
        start_periods = args.start_periods
    if not isinstance(args.end_periods, list):
        end_periods = [args.end_periods]
    else:
        end_periods = args.end_periods

    SETTINGS.update(provider=args.provider)
    SETTINGS.update(strategy_type=StrategyType[args.strategy_type])
    SETTINGS.update(ticker=args.symbol)
    SETTINGS.update(timeframe=args.timeframe)
    SETTINGS.update(alt_timeframe=args.alt_timeframe)
    SETTINGS.update(start_date=datetime.datetime.strptime(args.start_time, "%d.%m.%Y %H:%M"))
    SETTINGS.update(end_date=datetime.datetime.strptime(args.end_time, "%d.%m.%Y %H:%M") if args.end_time else datetime.datetime.utcnow())
    SETTINGS.update(start_deposit=args.deposit)
    SETTINGS.update(commission=args.commission)
    SETTINGS.update(trailing_stop_perc=args.trailing_stop_perc)
    SETTINGS.update(min_profit_for_stop=args.min_profit_for_stop)
    SETTINGS.update(profit_price_divider=args.profit_price_divider)
    SETTINGS.update(save_signals=args.save_signals)
    SETTINGS.update(save_signals_grouped=args.save_signals_grouped)
    SETTINGS.update(draw_plot=args.draw_plot)
    SETTINGS.update(max_period=max(end_periods))
    SETTINGS.update(reenter=args.reenter)
    SETTINGS.update(pyramiding=args.pyramiding)
    SETTINGS.update(stop_variant=args.stop_variant)

    print(f"Symbol: {SETTINGS['ticker']}")
    print(f"Timeframe: {str(SETTINGS['timeframe'])}")
    print(f"Start periods: {', '.join([str(p) for p in start_periods])}")
    print(f"End periods: {', '.join([str(p) for p in end_periods])}")
    print(f"Strategy: {SETTINGS['strategy_type'].get_name()}")
    print(f"Start time: {SETTINGS['start_date'].strftime('%d.%m.%Y %H:%M:%S')}")
    print(f"End time: {SETTINGS['end_date'].strftime('%d.%m.%Y %H:%M:%S')}")
    print(f"Start deposit: {SETTINGS['start_deposit']}")
    print(f"Commission: {SETTINGS['commission']}")
    print(f"Provider: {SETTINGS['provider']}")
    print(f"Trailing price percent: {SETTINGS['trailing_stop_perc']}")
    print(f"Minimal profit for stop: {SETTINGS['min_profit_for_stop']}")
    print(f"Profit price divider: {SETTINGS['profit_price_divider']}")
    print(f"Save signals: {SETTINGS['save_signals']}")
    print(f"Save grouped signals: {SETTINGS['save_signals_grouped']}")
    print(f"Draw plot: {SETTINGS['draw_plot']}")
    print(f"Pyramiding: {SETTINGS['pyramiding']}")
    print(f"Stop variant: {SETTINGS['stop_variant']}")
    print("Number of cpus: ", num_cpus)
    print("Number of computers: ", args.processes_limit)
    print()

    print(f"{datetime.datetime.now().strftime('%d.%m.%Y %H:%M')} Loading candles...")
    # Get candles
    if SETTINGS['provider'] == "BITFINEX":
        if args.update:
            cr = BFXCandleRequester(SETTINGS['ticker'], SETTINGS['start_date'] - datetime.timedelta(hours=SETTINGS['max_period']), SETTINGS['end_date'])
            cr.request_candles()

        cf = BFXCandleFormer(SETTINGS['ticker'], SETTINGS['timeframe'], SETTINGS['start_date'] - datetime.timedelta(hours=SETTINGS['max_period']), SETTINGS['end_date'])
        candles = cf.get_raw_candles()
    elif SETTINGS['provider'] == "BINANCE":
        if args.update:
            cr = BNCCandleRequester(SETTINGS['ticker'], SETTINGS['start_date'] - datetime.timedelta(hours=SETTINGS['max_period']), SETTINGS['end_date'])
            cr.request_candles()

        cf = BNCCandleFormer(SETTINGS['ticker'], SETTINGS['timeframe'], SETTINGS['start_date'] - datetime.timedelta(hours=SETTINGS['max_period']), SETTINGS['end_date'])
        candles = cf.get_raw_dict_candles()

    print(f"{datetime.datetime.now().strftime('%d.%m.%Y %H:%M')} Creating args list...")
    candles_obj_id = ray.put(candles)
    computer_args = []
    for x in range(start_periods[0], end_periods[0]):
        if len(start_periods) == 1 and len(end_periods) == 1:
            computer_args.append((candles_obj_id, (x,)))
        else:
            for y in range(start_periods[1], end_periods[1]):
                if x != y and x > y:
                    if len(start_periods) == 3:
                        for z in range(start_periods[2], end_periods[2]):
                            computer_args.append((candles_obj_id, (x, y, z)))
                    else:
                        computer_args.append((candles_obj_id, (x, y)))

    program_start = time.time()
    print(f"{datetime.datetime.now().strftime('%d.%m.%Y %H:%M')} Creating futures...")

    if args.processes_limit > 0:
        # execute with ray
        computers = []
        for i in range(args.processes_limit):
            computers.append(RayComputer.remote(SETTINGS))

        futures = []
        c = 0
        for arg in computer_args:
            futures.append(computers[c].compute_one_cycle.remote(arg))
            c += 1
            if c >= args.processes_limit:
                c = 0

        print(f"{datetime.datetime.now().strftime('%d.%m.%Y %H:%M')} Computing data...")
        ready_ids, remaining_ids = [], futures

        all_len = len(futures)
        while remaining_ids:
            ready_ids, remaining_ids = ray.wait(futures, num_returns=all_len, timeout=1.0)
            ready_len = len(ready_ids)
            remaining_len = len(remaining_ids)
            percent_ready = (ready_len / all_len * 100) if ready_len else 0
            cprint(f"{percent_ready:.2f}%", 'green', None, ['bold'], end=" ")
            print(f"| Ready: {ready_len}, Remaining: {remaining_len}, All: {all_len}", end="\r")

        results = ray.get(ready_ids)

    else:
        # execute without ray
        print(f"{datetime.datetime.now().strftime('%d.%m.%Y %H:%M')} Computing data...")
        results = []
        computer = Computer(SETTINGS)
        for arg in computer_args:
            results.append(computer.compute_one_cycle(arg))

    program_end = time.time()

    print(f"{datetime.datetime.now().strftime('%d.%m.%Y %H:%M')} Saving results...")
    with open(output, "w") as file:
        columns = ["period", "net profit", "frozen deposit", "max drawdown", "gross profit", "gross loss", "profit factor", "fee paid", "total trades", "avg winning trade", "avg loss trade", "% profitable", "avg trade"]
        writer = DictWriter(file, columns)
        writer.writeheader()

        for data in results:
            writer.writerow(data)

    print("Execution time:", (program_end - program_start), 'sec')
