FROM python:3.7-stretch
MAINTAINER Flowelcat <flowelcat@gmail.com>

# Installing git
RUN apt install git

RUN mkdir /backtester
COPY . /backtester
WORKDIR /backtester/

# Installing botmanlib and clearing dependencies
RUN pip install --upgrade --force-reinstall -r requirements.txt && pip install -e .

CMD mkdir /backtester/resources
CMD python /backtester/src/app.py
