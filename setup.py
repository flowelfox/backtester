import os

from setuptools import setup

VERSION = "1.0.1"

def install_deps():
    default = open('requirements.txt', 'r').readlines()
    new_pkgs = []
    for resource in default:
        if 'git+ssh' in resource or 'git+https' in resource:
            pass
        else:
            new_pkgs.append(resource.strip())
    return new_pkgs


pkgs = install_deps()

setup(name='Backtester',
      version=VERSION,
      description='Backtesting console application',
      author='Flowelcat',
      author_email='flowelcat@icloud.com',
      url='www.example.com',
      packages=['src', 'indicators'],
      install_requires=pkgs)


if not os.path.exists('logs'):
    os.mkdir('logs')

