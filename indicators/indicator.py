class Indicator:
    def __init__(self):
        self._candles = []
        self._value = 0

    def add(self, candle):
        self._candles.append(candle)
        self.recalculate()

    def value(self):
        return self._value

    def recalculate(self):
        raise NotImplementedError

    def cut_candles(self):
        if len(self._candles) > self.period:
            del self._candles[0]
        # elif len(self._candles) < self.period:
        #     return

class IndicatorDataInvalidError(Exception):
    def __init__(self, message=None, indicator="Unknown"):
        self.message = f"Indicator data invalid error in {indicator}"
        super(Exception, self).__init__(message)

    def __str__(self):
        return self.message
