from math import sqrt

from indicators.indicator import Indicator
from indicators.wma import WMA


class HMA(Indicator):
    def __init__(self, period, source='close'):
        super(HMA, self).__init__()
        self.period = period
        self.inner_1 = WMA(round((period / 2) + 1e-10), source=source)
        self.inner_2 = WMA(period, source=source)
        self.outer = WMA(round(sqrt(period)), source='custom')

    def add(self, candle):
        self._candles.append(candle)
        self.cut_candles()

        self.inner_1.add(candle)
        self.inner_2.add(candle)

        self.outer.add({'custom': 2 * self.inner_1.value() - self.inner_2.value()})
        self.recalculate()

    def recalculate(self):
        self._value = self.outer.value()

    def value(self):
        return super(HMA, self).value()
