import datetime

from indicators.indicator import IndicatorDataInvalidError, Indicator


class ZigZag(Indicator):
    def __init__(self, period, tf, alt_tf, alt_length=10):
        super(ZigZag, self).__init__()
        self.period = period
        self.alt_length = alt_length
        self._alt_candles = []
        self.timeframe = tf
        self.alt_timeframe = alt_tf
        self.last_zig_zag_time = None
        self.p_is_up = None
        self.p_is_down = None
        self.p_direction = None
        self.zig_zags = []
        self._value = (0, 0, 0, 0, 0, 0)

    def add(self, candle, is_alt=False):
        if is_alt:
            self._alt_candles.append(candle)
        else:
            self._candles.append(candle)
        ready = True

        if len(self._candles) > self.period + 1:
            del self._candles[0]
        elif len(self._candles) < self.period:
            ready = False

        if len(self._alt_candles) > self.alt_length + 1:
            del self._alt_candles[0]
        elif len(self._alt_candles) < self.alt_length:
            ready = False

        if ready:
            self.recalculate()

    def recalculate(self):
        if self.last_zig_zag_time is None:
            print(self.last_alt_candle['open_date'], self.last_alt_candle_close_time)
            self.last_zig_zag_time = self.last_alt_candle_close_time

        open = self.last_alt_candle['open']
        close = self.last_alt_candle['close']

        # find gap candles
        gap_candles = []
        for ac in self._alt_candles:
            if ac['open_date'] >= self.last_zig_zag_time:
                gap_candles.append(ac)

        is_up = close >= open
        is_down = close <= open
        if self.p_is_up and is_down:
            direction = False
        elif self.p_is_down and is_up:
            direction = True
        else:
            direction = None
        if not gap_candles:
            new_zig_zag = None
        elif self.p_is_up and is_down and self.p_direction != False:
            new_zig_zag = max([c['high'] for c in gap_candles])
        elif self.p_is_down and is_up and self.p_direction != True:
            new_zig_zag = min([c['low'] for c in gap_candles])
        else:
            new_zig_zag = None

        if self.last_alt_candle_close_time == self.last_candle_close_time and new_zig_zag is not None:
            self.zig_zags.append(new_zig_zag)
            self.last_zig_zag_time = self.last_alt_candle_close_time

        if len(self.zig_zags) > 5:
            del self.zig_zags[0]

        if len(self.zig_zags) == 5:
            x, a, b, c, d = self.zig_zags
            xab = (abs(b - a) / abs(x - a))
            xad = (abs(a - d) / abs(x - a))
            abc = (abs(b - c) / abs(a - b))
            bcd = (abs(c - d) / abs(b - c))

            self._value = (d, c, xab, xad, abc, bcd)
        else:
            self._value = (0, 0, 0, 0, 0, 0)

        # post save
        self.p_is_up = is_up
        self.p_is_down = is_down
        self.p_direction = direction

    @property
    def last_alt_candle(self):
        return self._alt_candles[-1]

    @property
    def last_candle(self):
        return self._candles[-1]

    @property
    def last_candle_close_time(self):
        close_time = (self.last_candle['open_date_dt'] + datetime.timedelta(minutes=self.timeframe)).timestamp() * 1000
        return int(close_time)

    @property
    def last_alt_candle_close_time(self):
        close_time = (self.last_alt_candle['open_date_dt'] + datetime.timedelta(minutes=self.alt_timeframe)).timestamp() * 1000
        return int(close_time)
