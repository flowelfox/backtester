from indicators.indicator import Indicator


class WMA(Indicator):
    def __init__(self, period, source='close'):
        super(WMA, self).__init__()
        self.period = period
        self.source = source

    def add(self, candle):
        self._candles.append(candle)
        self.cut_candles()

    def recalculate(self):
        weight = sum(list(range(len(self._candles) + 1)))
        if self.source == 'hl2':
            self._value = sum([((c['high'] + c['low']) / 2) * ((idx + 1) / weight) for idx, c in enumerate(self._candles)])
        else:
            self._value = sum([c[self.source] * ((idx + 1) / weight) for idx, c in enumerate(self._candles)])

    def value(self):
        self.recalculate()
        return self._value
