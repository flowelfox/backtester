from indicators.hma import HMA
from indicators.indicator import Indicator
from indicators.sma import SMA


class HMADIR(Indicator):
    def __init__(self, period, source='close'):
        super(HMADIR, self).__init__()
        self.period = period
        self.hma = HMA(period, source=source)
        self.sma = SMA(2, source='custom')

    def add(self, candle):
        self._candles.append(candle)

        self.hma.add(candle)
        self.sma.add({'custom': self.hma.value()})

        self.recalculate()
        return candle

    def recalculate(self):
        if len(self._candles) > 1:
            self._value = self.hma.value() - self.sma.value()
