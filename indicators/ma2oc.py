from indicators.indicator import Indicator, IndicatorDataInvalidError
from indicators.sma import SMA


class MA2OC(Indicator):
    def __init__(self, period):
        super(MA2OC, self).__init__()
        self.period = period

        self.open_sma = SMA(period, source='open')
        self.close_sma = SMA(period, source='close')

    def add(self, candle):
        self._candles.append(candle)

        self.open_sma.add(candle)
        self.close_sma.add(candle)
        self.recalculate()

    def recalculate(self):
        if len(self._candles) >= self.period:
            self._value = self.open_sma.value() - self.close_sma.value()

    def value(self):
        if not self._value:
            raise IndicatorDataInvalidError(indicator='MA2OC')

        return self._value
